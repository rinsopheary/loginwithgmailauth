import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableWithoutFeedback, Keyboard } from 'react-native'
import { Button } from 'native-base'
import auth from '@react-native-firebase/auth';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  onLogin() {
    auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        console.log('User account created & signed in!');
        alert('Login success')
        this.props.navigation.navigate("Homepage")
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  }

  onSignUp() {
    this.props.navigation.navigate("Signup")
  }

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => Keyboard.dismiss()}
      >
        <View style={styles.container}>
          <View style>
            <Text style={styles.inputext}> HOLY </Text>
          </View>
          <TextInput
            value={this.state.email}
            onChangeText={(email) => this.setState({ email })}
            placeholder='Email...'
            placeholderTextColor='white'
            style={styles.input}
            returnKeyType="go"
            autoCapitalize={false}
          />
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({ password })}
            placeholder='Password...'
            placeholderTextColor='white'
            secureTextEntry={true}
            style={styles.input}
            returnKeyType="done"
            onSubmitEditing={() => this.onLogin()}
          />

          <Button full style={styles.btnStyle} danger
            onPress={() => this.onLogin()}
          >
            <Text style={{ color: 'white', fontWeight: "bold" }}>LOGIN</Text>
          </Button>

          <Button full style={styles.btnStyle} danger
            onPress={() => this.onSignUp()}
          >
            <Text style={{ color: 'white', fontWeight: "bold" }}>SIGN UP</Text>
          </Button>

        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 45
  },
  input: {
    // width: 200,
    // height: 44,
    width: '100%',
    padding: 15,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
    borderRadius: 20,
    backgroundColor: 'black',
    color: 'white'
  },
  inputext: {
    width: '100%',
    fontSize: 50,
    fontWeight: 'bold',
    color: '#f05454',
    textAlign: "center",
    alignContent: "center",
    marginBottom: 30
  },
  btnStyle: {
    marginTop: 30,
    borderRadius: 20,
  }
})
