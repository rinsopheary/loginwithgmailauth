import React, { Component, useEffect } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import firestore from '@react-native-firebase/firestore';

export default class HomePage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             messages : [
                {
                    _id: 0,
                    text: 'New room created.',
                    createdAt: new Date().getTime(),
                    system: true
                  },
                  // example of chat message
                  {
                    _id: 1,
                    text: 'Hey bopha!',
                    createdAt: new Date().getTime(),
                    user: {
                      _id: 2,
                      name: 'Dara kok'
                    }
                }
             ]
        }
    }

    handlerSend = async (newMessage = []) => {
        // this.setState({
        //     messages :(GiftedChat.append(this.state.messages, newMessage))
        // });
        useEffect(() => {
            const messagesListener = firestore()
              .collection('THREADS')
              .doc('1')
              .collection('MESSAGES')
              .orderBy('createdAt', 'desc')
              .onSnapshot(querySnapshot => {
                const messages = querySnapshot.docs.map(doc => {
                  const firebaseData = doc.data();
        
                  const data = {
                    _id: doc.id,
                    text: '',
                    createdAt: new Date().getTime(),
                    ...firebaseData
                  };
        
                  if (!firebaseData.system) {
                    data.user = {
                      ...firebaseData.user,
                      name: firebaseData.user.email
                    };
                  }
        
                  return data;
                });
        
                // setMessages(messages);
                  this.setState({
            messages :(GiftedChat.append(this.state.messages, newMessage))
        });
              });
        
            return () => messagesListener();
          }, []);
    }
    
    render() {
        return (
            <GiftedChat
                messages={this.state.messages}
                onSend={newMessage => this.handlerSend(newMessage)}
                user={{ _id: 1 }}
            />
        )
    }
}

const styles = StyleSheet.create({})
