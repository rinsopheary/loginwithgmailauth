import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, TouchableWithoutFeedback, Keyboard } from 'react-native'
import { Button} from 'native-base'
import auth from '@react-native-firebase/auth';

export default class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            email: '',
            password: '',
        };
    }

    handlerSignup() {
        auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((error) => {
                console.log('User account created & signed in!');
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                    alert(error.code)
                }

                if (error.code === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                    alert(error.code)
                }

                console.error(error);
                alert(error.code)
            });

    }

    onBack() {
        this.props.navigation.navigate("Login")
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => Keyboard.dismiss()}
            >
                <View style={styles.container}>
                    <View style>
                        <Text style={styles.inputext}> Register </Text>
                    </View>

                    <TextInput
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}
                        placeholder='Full name ...'
                        placeholderTextColor='white'
                        style={styles.input}
                        returnKeyType="go"
                        value={this.state.username}
                        autoCapitalize={false}
                    />
                    <TextInput
                        value={this.state.email}
                        onChangeText={(email) => this.setState({ email })}
                        placeholder='Email...'
                        placeholderTextColor='white'
                        style={styles.input}
                        returnKeyType="go"
                        autoCapitalize={false}
                        value={this.state.email}
                        autoCapitalize={false}
                    />
                    <TextInput
                        value={this.state.password}
                        onChangeText={(password) => this.setState({ password })}
                        placeholder='Password...'
                        placeholderTextColor='white'
                        secureTextEntry={true}
                        style={styles.input}
                        returnKeyType="done"
                        onSubmitEditing={() => this.onSignup()}
                        value={this.state.password}
                        autoCapitalize={false}
                    />

                    <Button full style={styles.btnStyle} danger
                        onPress={() => this.handlerSignup()}
                    >
                        <Text style={{ color: 'white', fontWeight: "bold" }}>REGISTER</Text>
                    </Button>

                    <Button full style={styles.btnStyle} danger
                        onPress={() => this.onBack()}
                    >
                        <Text style={{ color: 'white', fontWeight: "bold" }}>BACK TO LOGIN</Text>
                    </Button>

                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 45
    },
    input: {
        // width: 200,
        // height: 44,
        width: '100%',
        padding: 15,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 20,
        backgroundColor: 'black',
        color: 'white'
    },
    inputext: {
        width: '100%',
        fontSize: 50,
        fontWeight: 'bold',
        color: '#f05454',
        textAlign: "center",
        alignContent: "center",
        marginBottom: 30
    },
    btnStyle: {
        marginTop: 30,
        borderRadius: 20,
    }
})

