export const fetchArticle = () => {
    return (dispatch) => {
        fetch(`http://110.74.194.124:15011/v1/api/articles?page=1&limit=15`,{
            method : 'GET',
            headers : {
                "Authorization" : 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
        .then((res)=>{
            return res.json()
        })
        .then((result) =>{
            return dispatch({
                type: 'GET_DATA',
                payload : result.data
            })
        })
        .catch((error)=>{
            console.log(error);
        })
    }
}