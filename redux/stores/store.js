import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { articleReducer } from "../reducers/ArticleReducer";

const middleware = [thunk]
export const myStore = createStore(articleReducer, compose(
    applyMiddleware(...middleware)
))