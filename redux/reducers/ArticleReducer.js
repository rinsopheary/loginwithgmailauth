const initState = {
    articles : []
}

export const articleReducer = (state = initState, action) => {
    switch(action.type){
        case 'GET_DATA':
            return ({
                ...state, 
                articles : action.payload
            }) 
        default : 
            return state
    }
}