// import React, { Component } from 'react'
// import { Text, StyleSheet, View } from 'react-native'
// import { Provider } from 'react-redux'
// import Home from './components/Home'
// import { myStore } from './redux/stores/store'

// export default class App extends Component {
//   render() {
//     return (
//       <Provider store={myStore}>
//         <Home />
//       </Provider>
//     )
//   }
// }

// const styles = StyleSheet.create({})

import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
// import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './realChat/login/Login';
import Signup from './realChat/signup/Signup';
import HomePage from './realChat/components/HomePage'
// import GroupChatting from './src/components/Chat/GroupChatting';

const Stack = createStackNavigator();

export default class App extends Component {

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>


        <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: null }}
          />
          <Stack.Screen
            name="Signup"
            component={Signup}
            options={{ headerShown: null }}
          />

         

          <Stack.Screen 
            name = "Homepage"
            component = {HomePage}
            options = {{headerShown : true}}
          />

        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({})

