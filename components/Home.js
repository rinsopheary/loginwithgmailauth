import React, { Component } from 'react'
import { Text, StyleSheet, View, FlatList, SafeAreaView } from 'react-native'
import { connect } from 'react-redux';
import {fetchArticle} from '../redux/actions/ArticleAction'


export class Home extends Component {
    
    componentDidMount(){
        // console.log(this.props.fetchArticle());

        console.log('Work');
        this.props.fetchArticle();
        // console.log(this.props.articles);
    
    }
    render() {
        console.log('all data = ',this.props.articles);
        return (
            <SafeAreaView>
                <FlatList
                    data = {this.props.articles}
                    renderItem = {({item})=>(
                        <Text>{item.title}</Text>
                    )}
                >

                </FlatList>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

})

const mapStateToProps = (state) => {

    // console.log('mapState work');
    // console.log(state);
    return {
        articles : state.articles
    }
}

export default connect(mapStateToProps, {fetchArticle}) (Home)


